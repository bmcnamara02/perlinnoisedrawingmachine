//
//  NoiseMachine.h
//  DrawingMachine_1_PerlinNoise
//
//  Created by Barry McNamara on 04/09/2015.
//
//

#ifndef __DrawingMachine_1_PerlinNoise__NoiseMachine__
#define __DrawingMachine_1_PerlinNoise__NoiseMachine__

#include "ofMain.h"

class NoiseMachine {
public:
    
    void setup();
    void draw();
    void update();
    void clear();
    ofVec2f getField(ofVec2f position);
    
    vector<ofVec2f> points;
    ofMesh cloud;
    float t;
    float width, height;
    
    int nPoints = 17000; // points to draw
    float complexity = 12; // wind complexity
    float pollenMass = .8; // pollen mass
    float timeSpeed = .02; // wind variation speed
    float phase = TWO_PI; // separate u-noise from v-noise
    
    ofParameterGroup noiseParameters;
    ofParameter<float> noiseComplexity;
    ofParameter<float> noisePollenMass;
    ofParameter<float> noiseTimeSpeed;
    ofParameter<float> phaseDivider;
    

};

#endif /* defined(__DrawingMachine_1_PerlinNoise__NoiseMachine__) */
