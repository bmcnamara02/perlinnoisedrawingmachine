#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetVerticalSync(true); // don't go too fast
    ofEnableAlphaBlending();
    
    noiseMachine.setup();
    parameterGroup.add(noiseMachine.noiseParameters);
    gui.setup(parameterGroup);
}

//--------------------------------------------------------------
void ofApp::update(){
    noiseMachine.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
//    ofColor centerColor = ofColor(216, 216, 210);
//    ofColor edgeColor(229, 227, 220);
//    ofBackgroundGradient(centerColor, edgeColor, OF_GRADIENT_CIRCULAR);
    
    ofBackground(10, 15, 10);
    noiseMachine.draw();
    gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch (key) {
        case 'f':
            ofToggleFullscreen();
            break;
        }
    
    if (key == ' ') {
        noiseMachine.clear();
    }

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
