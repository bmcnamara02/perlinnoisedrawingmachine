//
//  NoiseMachine.cpp
//  DrawingMachine_1_PerlinNoise
//
//  Created by Barry McNamara on 04/09/2015.
//
//

#include "NoiseMachine.h"

//--------------------------------------------------------------
ofVec2f NoiseMachine::getField(ofVec2f position) {
    float normx = ofNormalize(position.x, 0, ofGetWidth());
    float normy = ofNormalize(position.y, 0, ofGetHeight());
    float u = ofNoise(t + phase / phaseDivider, normx * noiseComplexity + phase / phaseDivider, normy * noiseComplexity + phase / phaseDivider);
    float v = ofNoise(t - phase / phaseDivider, normx * noiseComplexity - phase / phaseDivider, normy * noiseComplexity + phase / phaseDivider);
    return ofVec2f(u, v);
}

//--------------------------------------------------------------
void NoiseMachine::setup() {

    // SET UP UI
    noiseParameters.setName("Noise Control");
    noiseParameters.add(noiseComplexity.set("Complexity", 12, 0.5, 15));
    noiseParameters.add(noisePollenMass.set("Pollen Mass", 0.8, 0.01, 1.0));
    noiseParameters.add(noiseTimeSpeed.set("Time Speed", 0.02, 0.001, 0.1));
    noiseParameters.add(phaseDivider.set("Phase", 1, 1, 25));
    
    // randomly allocate the points across the screen
    points.resize(nPoints);
    for(int i = 0; i < nPoints; i++) {
        points[i] = ofVec2f(ofRandom(0, ofGetWidth()), ofRandom(0, ofGetHeight()));
    }
    
    // we'll be drawing the points into an ofMesh that is drawn as bunch of points
    cloud.clear();
    cloud.setMode(OF_PRIMITIVE_POINTS);
}

//--------------------------------------------------------------
void NoiseMachine::update() {
    width = ofGetWidth(), height = ofGetHeight();
    t = ofGetFrameNum() * noiseTimeSpeed;
    for(int i = 0; i < nPoints; i++) {
        float x = points[i].x, y = points[i].y;
        ofVec2f field = getField(points[i]); // get the field at this position
        // use the strength of the field to determine a speed to move
        // the speed is changing over time and velocity-space as well
        float speed = (2.5 + ofNoise(t, field.x, field.y)) / noisePollenMass;
        // add the velocity of the particle to its position
        x += ofLerp(-speed, speed, field.x);
        y += ofLerp(-speed, speed, field.y);
        // if we've moved outside of the screen, reinitialize randomly
        if(x < 0 || x > width || y < 0 || y > height) {
            x = ofRandom(0, width);
            y = ofRandom(0, height);
        }
        // save the changes we made to the position
        points[i].x = x;
        points[i].y = y;
        // add the current point to our collection of drawn points
        cloud.addVertex(ofVec2f(x, y));
    }
}

//--------------------------------------------------------------
void NoiseMachine::draw() {

        // when not in debug mode, draw all the points to the screen
        ofSetColor(216, 216, 210, 5);
        cloud.draw();

    
}

void NoiseMachine::clear() {
    points.resize(nPoints);
    for(int i = 0; i < nPoints; i++) {
        points[i] = ofVec2f(ofRandom(0, ofGetWidth()), ofRandom(0, ofGetHeight()));
    }
    
    // we'll be drawing the points into an ofMesh that is drawn as bunch of points
    cloud.clear();
    cloud.setMode(OF_PRIMITIVE_POINTS);

}
